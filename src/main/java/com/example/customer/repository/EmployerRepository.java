package com.example.customer.repository;

import com.example.customer.entities.Employer;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EmployerRepository extends MongoRepository<Employer, Integer> {

    Employer findAllById(int id);

    void deleteAllByCustomerId(int customerId);

}
