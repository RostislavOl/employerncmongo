package com.example.customer.Controllers;


import com.example.customer.entities.Employer;
import com.example.customer.repository.EmployerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping(path = "/employers")
public class EmployerController {

    @Autowired
    EmployerRepository employers;

    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<?> getAll() {
        return ResponseEntity.ok(employers.findAll());
    }

    @RequestMapping(method = RequestMethod.GET, path = "/{id}")
    public ResponseEntity<?> getUserById(@PathVariable int id) {
        Employer employer = employers.findAllById(id);
        if (employer != null) {
            return ResponseEntity.ok(employer);
        }
        return ResponseEntity.status(HttpStatus.NO_CONTENT).body("User with id = " + id + " not found");
    }

    @RequestMapping(method = RequestMethod.PUT)
    public ResponseEntity<?> createEmployer(@RequestBody Employer employer) {
        if (employer != null) {
            Employer employerInRepository = employers.findAllById(employer.getEmployerId());
            //обновление
            if (employerInRepository != null) {
                employers.delete(employerInRepository.getEmployerId());
                employerInRepository.setCustomerId(employer.getCustomerId());
                employerInRepository.setFirstName(employer.getFirstName());
                employerInRepository.setLastName(employer.getLastName());
                employerInRepository.setEmail(employer.getEmail());
                employerInRepository.setJobRole(employer.getJobRole());
                employers.save(employerInRepository);
                return ResponseEntity.ok().build();
            } else {
                employers.save(employer);
            }
        }
        try {
            return ResponseEntity
                    .created(new URI("/employers/" + employer.getEmployerId()))
                    .body(employer);
        } catch (URISyntaxException e) {
            return ResponseEntity.status(HttpStatus.CREATED).body(employer);
        }
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity deleteEmployer(@PathVariable int id) {
        Employer employer = employers.findAllById(id);
        if (employer != null) {
            employers.delete(id);
        }
        return ResponseEntity.ok().build();
    }

    @RequestMapping(value = "/{id}/customerId", method = RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity deleteEmployerByDept(@PathVariable int id) {
        System.out.println("/{id}/customerId");
        employers.deleteAllByCustomerId(id);
        return ResponseEntity.ok().build();
    }

}
